// 1.  What directive is used by Node.js in loading the modules it needs?
it uses the require  directive to load modules

// 2 . What Node.js module contains a method for server creation?
the http module

// 3.  What is the method of the http object responsible for creating a server using Node.js?
the createServer() method

// 4. - What method of the response object allows us to set status codes and content types?
the writeHead() method

// 5. Where will console.log() output its contents when run in Node.js?
in the terminal

// 6.  What property of the request object contains the address's endpoint?

the url